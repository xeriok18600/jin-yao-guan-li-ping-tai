# 金鑰管理平台(2020/12 ~ 2021/01) #

![登入畫面](https://cdn.discordapp.com/attachments/292622123543953408/812233233122197554/FireShot_Capture_964_-_-_-_10.11.101.99.png)

### 開發環境 ###

1. Pug
2. Sass
3. Vue
4. Vuetify
5. Axios
6. Lottie
7. Chart

### 簡介 ###

* 可快速設定產品與金鑰，簡約設計方便查看重要資訊
* 此為管理者帳號介面

### 目錄 ###

1. 儀錶版
2. 設定(DEMO 產品設定)
3. 金鑰管理

---------------------------------------------------

### 1.儀錶版 ###

![儀錶版](https://cdn.discordapp.com/attachments/292622123543953408/812229281676525598/FireShot_Capture_958_-_-_-_10.11.101.99.png)


### 2.設定(DEMO 產品設定) ###

![刪除特效](https://cdn.discordapp.com/attachments/292622123543953408/812234920230649896/1611742948635.gif)
結合 Lottie.js 顯示特效

### 3.金鑰管理 ###

![金鑰管理](https://cdn.discordapp.com/attachments/292622123543953408/812234572251004958/FireShot_Capture_961_-_-_-_10.11.101.99.png)
利用表格擴充清楚顯示綁定設備與金鑰的關聯性
